// This #include statement was automatically added by the Particle IDE.
#include <SparkJson.h>

// This #include statement was automatically added by the Particle IDE.
#include "OBDMessage.h"
#include "base85.h"

// This #include statement was automatically added by the Particle IDE.
#include <HttpClient.h>

//Vin Reader Stuff
#include "carloop.h"

#define OBD_CAN_BROADCAST_ID 0x7DF
void readVIN();
int startReadVIN(String unused);
// Set up the Carloop hardware
Carloop<CarloopRevision2> carloop;


unsigned int nextTime = 0;
HttpClient http;

String SERVER_NAME = "car.jonathonmorris.co.uk";
String SERVER_PATH = "/index.php/DCUpload/";

String action;

http_request_t request;
http_response_t response;

http_header_t headers[] = {
    //{"Host","car.jonathonmorris.co.uk"},
    { "Content-Type", "application/x-www-form-urlencoded" },
    //  { "Accept" , "application/json" },
    { "Accept" , "*/*"},
    {"User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"},
    { NULL, NULL } // NOTE: Always terminate headers will NULL
};

String var_VIN = "YV1MV5151E2081550"; 
String x04 = "everything";

String var_RVIN = ""; 

void setup()
{
  Particle.process();
  Particle.publish("Debug/Status/setup()","bootting");
  carloop.begin();
  Particle.publish("Debug/Status/setup()","carloop.being()");
  Particle.function("readVIN", startReadVIN);
  Particle.publish("Debug/Status/setup()","particle.function readVIN");
  readVIN();
  Particle.publish("Debug/Status/setup()","readVIN() Called");
  Particle.publish("Debug/Status/RVIN",var_RVIN);
  
  request.hostname = SERVER_NAME;
  request.path= SERVER_PATH+"bootcheck";
  request.body = "vin="+var_VIN;
  
  http.post(request, response, headers);
  Particle.publish("HTTP body",response.body);
  
  
  // Check if we need to pull everything from the car.
  if(response.body == "check"){
      Particle.publish("Status","Recheck Activated");
      //START OBD Codes
      
      
      
      
      
      
      //END OBD Codes
      request.path= SERVER_PATH+"data_dump";
      request.body = "vin="+var_VIN;
      
  }
  
  
  
  request.path= SERVER_PATH+"bootcheck";
  request.body = "vin="+var_VIN;
  http.post(request, response, headers);
  
  response.body;
  
  
  waitUntil(WiFi.ready);
  waitFor(Particle.connected, 10000);
}

void loop()
{
    
}

void readVIN() {
  Particle.publish("Debug/Status/readVIN()","starting");    //Make it here when the not connected to vehicle.

  // Send request to read VIN
  CANMessage message;
  Particle.publish("Debug/Status/readVIN()","Passed CANMessage Message");
  message.id = OBD_CAN_BROADCAST_ID;
  message.len = 8; // just always use 8A
  message.data[0] = 0x02; // 0 = single-frame format, 2  = num data bytes
  message.data[1] = 0x09;
  message.data[2] = 0x02;
  Particle.publish("Debug/Status/readVIN()","attempting to transmit message");
  carloop.can().transmit(message);
  Particle.publish("Debug/Status/readVIN()","transmit message");
  OBDMessage obd;
  unsigned long start = millis();
  unsigned long timeout = 200; // ms
  while(millis() - start < timeout) {
    if (carloop.can().receive(message)) {
    Particle.publish("Debug/Status/readVIN()","Received a Message");
      if (message.id == 0x7E8) {
        Particle.publish("Debug/Status/Got Reply from ECU");
        Serial.println("Got reply from ECU");
        Particle.publish("Debug/Status/Got Reply from ECU - Progress"); 

        // Add the data to our OBD message
        bool needsFlowControl = obd.addMessageData(message);

        if (needsFlowControl) {
          // Sending flow control
          CANMessage flowControl = obd.flowControlMessage();
          carloop.can().transmit(flowControl);
          Serial.println("Sent flow control");
          Particle.publish("Debug/Status/Sent Flow Control");
        }

        // Use the data when the message is complete
        if (obd.complete()) {
          String vin = "";
          // VIN is 17 character and can be left-padded with zeros
          for (int i = obd.size() - 17; i < obd.size(); i++) {
            vin += String((char)obd.data()[i]);
          }
          Serial.println("VIN: " + vin);
          if (Particle.connected()) {
            Particle.publish("vin/result", vin, PRIVATE);
            var_RVIN=vin;
          }

          obd.clear();
          Serial.println("Done");
          return;
        }
      }
    }
  }

  Serial.println("Timeout");
  if (Particle.connected()) {
    Particle.publish("vin/error", PRIVATE);
  }
}

int startReadVIN(String unused) {
  readVIN();
  return 0;
}